from SublimeLinter.lint import ComposerLinter
import json
import os

class Psalm(ComposerLinter):
    regex = r'^.*:(?P<line>[0-9]+):(?P<col>[0-9]+):(?:(?P<error>error)|(?P<warning>warning)) - (?P<message>(.(?!\(\S+\)$))+)$'
    defaults = { "selector": "source.php" }

    def split_match(self, match):
    	match, line, col, error, warning, message, near = super().split_match(match)
    	first_line_region = self.view.line(0)
    	first_line = self.view.substr(first_line_region)

    	if first_line != '<?php':
        	line = line - 1

    	return match, line, col, error, warning, message, near

    def cmd(self):
        command = ['psalm']
        command.append('--output-format=emacs')
        command.append('--monochrome')
        command.append('--no-progress')

        self.config_path = self.get_config_path()

        if os.path.isfile(self.config_path):
            command.append('--config=' + self.config_path)

        command.append('${args}')
        command.append('--')
        command.append('${file}')

        return command

    def get_config_path(self):
        """Get the path to the pslam.xml or psalm.xml.dist file for the current file."""
        filename = self.view.file_name()
        cwd = (
            os.path.dirname(filename) if filename else
            linter.guess_project_root_of_view(self.view)
        )

        return self.rev_parse_config_path(cwd) if cwd else None

    def rev_parse_config_path(self, cwd):
        """
        Search parent directories for pslam.xml or psalm.xml.dist.
        Starting at the current working directory. Go up one directory
        at a time checking if that directory contains a pslam.xml or psalm.xml.dist
        file. If it does, return that directory.
        """
        dist_config_path = os.path.normpath(os.path.join(cwd, 'psalm.xml.dist'))
        local_config_path = os.path.normpath(os.path.join(cwd, 'psalm.xml'))

        if os.path.isfile(local_config_path):
            return local_config_path

        if os.path.isfile(dist_config_path):
            return dist_config_path

        parent = os.path.dirname(cwd)

        if parent == '/' or parent == cwd:
            return None

        return self.rev_parse_config_path(parent)